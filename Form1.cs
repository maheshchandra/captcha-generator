﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Security.Cryptography;
using System.IO;

namespace Captcha_Generator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        List<string> strings = new List<string>();

        private void button2_Click(object sender, EventArgs e)
        {
            Image[] images = GenerateCaptchas(Convert.ToInt32(textBox1.Text));
            int g = 0;
            foreach (Image image in images)
            {
                image.Save(label1.Text + "\\" + strings[g] + ".png");
                g++;
            }

        }
        Image[] GenerateCaptchas(int amount)
        {
            Image[] images= new Image[amount];
            Random r = new Random();
            for (int z = 0; z < amount; z++)
            {
                Bitmap bitmap = new Bitmap(panel1.Width, panel1.Height);
                Graphics g = Graphics.FromImage(bitmap);
                g.Clear(panel1.BackColor);
                
                SolidBrush sb = new SolidBrush(Color.FromArgb(0xff, r.Next(0, 255), r.Next(0, 255), r.Next(0, 255)));
                Pen pen = new Pen(Color.FromArgb(0xff, r.Next(0, 255), r.Next(0, 255), r.Next(0, 255)));
                char[] chars = "asdsdfhdjkfhjkdshfjsfh12345215205mn".ToCharArray();
                string randomstring = "";
                for (int i = 0; i < 6; i++)
                {
                    randomstring += chars[r.Next(0, 35)];
                }
                byte[] buffer = new byte[randomstring.Length];
                int y = 0;
                foreach (char c in randomstring.ToCharArray())
                {
                    buffer[y] = (byte)c;
                    y++;
                }
                MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
                string md5string = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "");
                strings.Add(md5string);
                FontFamily ff = new FontFamily("Arial");
                Font font = new System.Drawing.Font(ff, 14);
                g.DrawString(randomstring, font, sb, 20, 20);
                for (int i = 0; i < 6; i++)
                {
                    int j = r.Next(0, 2);
                    if (j == 0)
                        g.DrawRectangle(pen, r.Next(0, 125), r.Next(0, 65), r.Next(0, 125), r.Next(0, 65));
                    else if (j == 1)
                        g.DrawEllipse(pen, r.Next(0, 125), r.Next(0, 65), r.Next(0, 125), r.Next(0, 65));
                    pen.Color = Color.FromArgb(0xff, r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
                }
                panel1.BackgroundImage = bitmap;

                images[z] = bitmap;
            }
            return images;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                label1.Text = fbd.SelectedPath;
            }
        }
        string Md5hashesname = "";
        private void button4_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.ImageLocation = ofd.FileName;
                Md5hashesname = Path.GetFileNameWithoutExtension(ofd.FileName);
            }
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] buffer = new byte[textBox2.Text.Length];
            int y = 0;
            foreach (char c in textBox2.Text.ToCharArray())
            {
                buffer[y] = (byte)c;
                y++;
            }
            string capthatext = BitConverter.ToString(md5.ComputeHash(buffer)).Replace("-", "");

            if (capthatext != Md5hashesname)
            {
                MessageBox.Show("Wrong");
            }
            else
            {
                MessageBox.Show("right");
            }

        }
    }
}
